﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wiggle : MonoBehaviour {
	Rigidbody2D rb;
	// Use this for initialization
	void Start () {
		rb = gameObject.GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Random.Range(0,60) == 0)
		{
			if (rb != null)
			{
				rb.AddTorque(Random.Range(-100f, 100f));
			}
		}

	}


}
