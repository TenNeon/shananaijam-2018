﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FireWall : MonoBehaviour {
	float travelDistance = 16f;
	[SerializeField]
	float travelTime = 15f;
	float distanceTraveled = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (distanceTraveled < travelDistance)
		{
			float distancePerFrame = Time.deltaTime * (travelDistance/travelTime);
			distanceTraveled += distancePerFrame;
			var pos = transform.position;
			pos.y -= distancePerFrame;
			transform.position = pos;
		}
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		var player = collision.gameObject.GetComponent<PlayerController>();
		if (player != null)
		{
			StartCoroutine("GameOver");
		}
	}

	IEnumerator GameOver()
	{
		Time.timeScale = 0.2f;
		yield return new WaitForSecondsRealtime(1f);
		Time.timeScale = 1f;
		Game.GameOver();
		SceneManager.LoadScene("Title");
	}
}
