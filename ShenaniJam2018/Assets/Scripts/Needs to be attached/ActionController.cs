﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionController : MonoBehaviour {
	public static ActionController main;
	public enum ActionState
	{
		Inactive,
		Delete,
		Paint
	}

	public static ActionState state;
	// Use this for initialization
	void Start () {
		if (main == null)
		{
			main = this;
		}
		else
		{
			Destroy(this);
		}
		state = new ActionState();
		SelectionController.SelectionChangeEvent += UpdateSelection;
	}
	
	// Update is called once per frame
	void Update () {
		KeyManager.UpdateTrackedKeys();
		if (KeyManager.GetKeyDown("rotate"))
		{
			WorldSpin.RotateCW();
		}
		if (KeyManager.GetKeyUp("rotate"))
		{
			WorldSpin.RotateCCW();
		}

		if (KeyManager.GetKey("Menu"))
		{
			Game.Quit();
		}
	}

	void UpdateSelection(string newSelection)
	{
		switch (newSelection)
		{
			case "delete action":
				state = ActionState.Delete;
				break;
			case "paint action":
				state = ActionState.Paint;
				break;
			default:
				state = ActionState.Inactive;
				break;
		}

		PaintAction.selectedSprite = SelectionController.GetSelectedSprite();
	}
}
