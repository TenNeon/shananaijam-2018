﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour {
	public bool hotbar = false;
	public bool keybindingMenu = false;
	public bool videoSettingsMenu = false;
	static float startTime = 0;

	// Use this for initialization
	void Start () {
		KeyManager.Init();

		if (hotbar)
		{
			Hotbar.Init();
		}
		if (keybindingMenu)
		{
			KeyBindingMenu.Init();
		}
		if (videoSettingsMenu)
		{
			VideoSettingsMenu.Init();
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public static void Quit()
	{
		Application.Quit();
	}

	public static void NewGame()
	{
		startTime = Time.time;
		SceneManager.LoadScene("level1");
	}

	public static void EndTimer()
	{
		float totalTime = Time.time - startTime;
		if (PlayerPrefs.HasKey("time"))
		{
			var storedTime = PlayerPrefs.GetFloat("Time");
			if (totalTime < storedTime)
			{
				PlayerPrefs.SetFloat("Time", totalTime);
				PlayerPrefs.Save();
			}
		}
		else
		{
			PlayerPrefs.SetFloat("Time", totalTime);
			PlayerPrefs.Save();
		}
		startTime = 0;
	}

	public static void GameOver()
	{
		startTime = 0;
	}

	public static void Win()
	{
		EndTimer();
	}

}
