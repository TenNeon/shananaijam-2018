﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PixelPerfect : MonoBehaviour {
	public float OrthographicSize;
	public int PixelsPerUnit = 32;
	public int PixelScale = 1;
	public bool UpdateCamera = false;

	// Use this for initialization
	void Update () {
		if (UpdateCamera)
		{
			RefreshSize();
			UpdateCamera = false;
		}
	}
	
	void RefreshSize () {
		CalculateCameraSize();
		Camera.main.orthographicSize = OrthographicSize;
	}

	void CalculateCameraSize()
	{
		OrthographicSize = (Screen.height / (PixelsPerUnit * PixelScale)) * 0.5f;
	}

}
