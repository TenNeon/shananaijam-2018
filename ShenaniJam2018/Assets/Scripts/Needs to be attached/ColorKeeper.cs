﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorKeeper : MonoBehaviour {
	public List<string> colorNamesIn;
	public List<Color> colorsIn;
	public static Dictionary<string, Color> colors;
	public static ColorKeeper main;

	private void Awake()
	{
		if (main == null)
		{
			main = this;
		}
		else
		{
			Destroy(this);
		}

		colors = new Dictionary<string, Color>();

		for (int i = 0; i < colorNamesIn.Count; i++)
		{
			if (!colors.ContainsKey(colorNamesIn[i]))
			{
				colors.Add(colorNamesIn[i], colorsIn[i]);
			}
		}
	}

	public static Color GetColor(string name)
	{
		if (colors.ContainsKey(name))
		{
			return colors[name];
		}
		else
		{
			return Color.cyan;
		}
	}
}
