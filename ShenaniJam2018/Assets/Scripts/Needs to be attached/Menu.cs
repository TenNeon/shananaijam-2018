﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour {
	GameObject menuObject;

	[SerializeField]
	int menuWidth = 200;
	[SerializeField]
	int menuHeight = 200;
	[SerializeField]
	RectOffset padding = new RectOffset();
	[SerializeField]
	int buttonHeight = 50;

	public string gameName = "Game Name";
	// Use this for initialization
	void Start () {
		BuildMenu();
	}

	private void BuildMenu()
	{
		UIUtility.AttachCanvas(gameObject, RenderMode.ScreenSpaceOverlay);
		UIUtility.Fill = ColorKeeper.colors["middlegreen"];
		UIUtility.FontSize = 36;
		UIUtility.Fit = false;
		UIUtility.Stroke = ColorKeeper.colors["darkgreen"];
		UIUtility.StrokeWidth = 4;
		UIUtility.Padding = new RectOffset(2, 2, 0,0);
		UIUtility.AttachCanvas(gameObject, RenderMode.ScreenSpaceOverlay);
		UIUtility.TextColor = ColorKeeper.colors["lightgreen"];
		
		menuObject = UIUtility.Box(new Rect(0,0, menuWidth, menuHeight), transform);
		var layout = menuObject.AddComponent<VerticalLayoutGroup>();
		layout.padding = padding;
		layout.spacing = 10;
		
		var titleObject = UIUtility.Text(menuObject.transform, gameName);

		UIUtility.FontSize = 24;
		SetupHighScore();
		UIUtility.FontSize = 36;

		UIUtility.Fill = Color.white;

		ColorBlock cb = new ColorBlock();
		cb.normalColor = ColorKeeper.colors["lightgreen"];
		cb.highlightedColor = ColorKeeper.colors["brightgreen"];
		cb.pressedColor = ColorKeeper.colors["middlegreen"];
		cb.colorMultiplier = 1f;
		UIUtility.ButtonColors = cb;
		UIUtility.TextColor = ColorKeeper.colors["darkgreen"];

		


		var continueButtonObject = UIUtility.Button(new Rect(0,0, menuWidth- padding.horizontal, buttonHeight), menuObject.transform, "Play");
		//var newGameButtonObject = UIUtility.Button(new Rect(0, 0, menuWidth - padding.horizontal, buttonHeight), menuObject.transform, "New Game");
		//var loadGameButtonObject = UIUtility.Button(new Rect(0, 0, menuWidth - padding.horizontal, buttonHeight), menuObject.transform, "Load Game");
		//var optionsButtonObject = UIUtility.Button(new Rect(0, 0, menuWidth - padding.horizontal, buttonHeight), menuObject.transform, "Options");
		var exitButtonObject = UIUtility.Button(new Rect(0, 0, menuWidth - padding.horizontal, buttonHeight), menuObject.transform, "Exit");

		continueButtonObject.GetComponent<Button>().onClick.AddListener(NewGame);
		exitButtonObject.GetComponent<Button>().onClick.AddListener(Quit);
	}

	void SetupHighScore()
	{
		if (PlayerPrefs.HasKey("Time"))
		{
			var timeScore = PlayerPrefs.GetFloat("Time");
			string scoreString = "Best time: " + timeScore.ToString("F2");
			UIUtility.Text(menuObject.transform, scoreString);
		}

	}

	public static void ShowMenu()
	{

	}

	public static void HideMenu()
	{

	}

	public static void ToggleMenu()
	{

	}

	void NewGame()
	{
		Game.NewGame();
	}

	void Quit()
	{
		Game.Quit();
	}
}
