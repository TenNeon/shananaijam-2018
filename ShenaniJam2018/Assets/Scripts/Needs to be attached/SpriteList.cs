﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SpriteList : MonoBehaviour {

	[SerializeField]
	private List<Sprite> sprites = new List<Sprite>();
	public static SpriteList main;

	public int SpriteCount { get { return sprites.Count; } }

	// Use this for initialization
	void Awake () {
		if (main == null)
		{
			main = this;    
		}
		else
		{
			Destroy(this);
		}
	}

	public Sprite GetSprite(string name)
	{
		Sprite foundSprite = 
		  (
			from found 
			in sprites
			where found.name == name
			select found
		).FirstOrDefault();

		if (foundSprite == null)
		{
			Debug.Log(string.Format("{0} not found in sprite list", name));
		}

		return foundSprite;
	}

	public Sprite GetSprite(int index)
	{
		if (index < sprites.Count && index >= 0)
		{
			return sprites[index];
		}
		return null;
	}

	public Sprite GetRandomSprite(int maxIndex = 63)
	{
		maxIndex = sprites.Count < maxIndex ? sprites.Count : maxIndex;
		int randomIndex = Random.Range(0, maxIndex);

		return sprites[randomIndex];
	}

	public List<Sprite> GetShuffledSpriteList(int maxIndex = 63)
	{
		maxIndex = sprites.Count < maxIndex ? sprites.Count : maxIndex;
		List<Sprite> shortenedList = sprites.Take(maxIndex).ToList<Sprite>();
		shortenedList = shortenedList.OrderBy((item) => Random.value).ToList();

		return shortenedList;
	}
}
