﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionController : MonoBehaviour {
	public delegate void SelectionChange(string newSelection);
	public static event SelectionChange SelectionChangeEvent;
	public static SelectionController main;
	public static string selected;


	// Use this for initialization
	void Awake () {
		if (main == null)
		{
			main = this;
		}
		else
		{
			Destroy(this);
		}

		selected = "";
		Hotbar.BoxSelectionEvent += ValueChanged;

	}

	void ValueChanged(string value)
	{
		selected = value;

		if (SelectionChangeEvent != null) 
		{
			SelectionChangeEvent(selected);
		}
	}

	public static Sprite GetSelectedSprite()
	{
		var foundSprite = SpriteList.main.GetSprite( Hotbar.GetSelectedBox().spriteName );
		return foundSprite;
	}
}
