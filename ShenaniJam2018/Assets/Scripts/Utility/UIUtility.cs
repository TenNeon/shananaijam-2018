﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIUtility
{
	static int strokeWidth = 1;
	static Color strokeColor = Color.white;
	static Color fillColor = Color.white;
	static bool fill = true;
	static bool stroke = true;
	static Font font = Resources.GetBuiltinResource<Font>("Arial.ttf");
	static int fontSize = 14;
	static RectOffset padding = new RectOffset(0,0,0,0);
	static Color textColor = Color.black;
	static EventSystem eventSystem;
	static bool fit = true;
	static ColorBlock buttonColors = new ColorBlock();

	public static void NoStroke() { stroke = false; }
	public static void NoFill() { fill = false; }

	public static RectOffset Padding { get { return padding; } set { padding = value; } }
	public static Color Stroke { get { return strokeColor; } set { strokeColor = value; stroke = true; } }
	public static int StrokeWidth { get { return strokeWidth; } set { strokeWidth = value; stroke = true; } }
	public static Color Fill { get { return fillColor; } set { fillColor = value; fill = true; } }
	public static Font Font { get { return font; } set { font = value; } }
	public static int FontSize { get { return fontSize; } set { fontSize = value; } }
	public static Color TextColor { get { return textColor; } set { textColor = value; } }
	public static bool Fit { get { return fit; } set { fit = value; } }
	public static ColorBlock ButtonColors { get { return buttonColors; } set { buttonColors = value; } }

	public static GameObject Button(Rect rect, Transform parent = null, string text = "")
	{
		GameObject box = Box(rect, parent, text);
		var button = box.AddComponent<Button>();
		button.colors = buttonColors;
		var nav = button.navigation;
		nav.mode = Navigation.Mode.None;
		button.navigation = nav;
		return box;
	}

	public static GameObject Box(Rect rect,Transform parent = null, string text = "")
	{
		GameObject go = new GameObject("Box");
		if (parent != null)
		{
			go.transform.SetParent(parent);
		}
		
		//fill
		Image img = go.AddComponent<Image>();
		var rt = img.rectTransform;
		rt.anchorMin = new Vector2(0.5f, 0.5f);
		rt.anchorMax = new Vector2(0.5f, 0.5f);
		rt.anchoredPosition = rect.center;
		rt.sizeDelta = new Vector2(rect.width, rect.height);
		//rt.pivot = new Vector2(0, 1);

		if (fill)
		{
			img.color = fillColor;
		}
		else
		{
			img.color = new Color(0,0,0,0);
		}

		//text
		if (text != "")
		{
			go.AddComponent<RectMask2D>();
			Text(go.transform, text);

			if (fit)
			{
				var fitter = go.AddComponent<ContentSizeFitter>();
				fitter.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
				fitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
			}

			var layout = go.AddComponent<VerticalLayoutGroup>();
			layout.padding = padding;
			layout.childAlignment = TextAnchor.MiddleCenter;
		}

		//outline
		if (stroke && strokeWidth > 0)
		{
			var outline = go.AddComponent<Outline>();
			outline.effectDistance = new Vector2(strokeWidth, strokeWidth);
			outline.effectColor = strokeColor;
		}


		rt.anchoredPosition = rect.position;
		return go;
	}

	public static GameObject Text(Transform parent = null, string inputText = "")
	{
		GameObject go = new GameObject("Text");
		if (parent != null)
		{
			go.transform.SetParent(parent);
		}

		var text = go.AddComponent<Text>();
		text.text = inputText;
		if (font != null)
		{
			text.font = font;
		}
		else
		{
			text.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
		}

		text.resizeTextForBestFit = false;
		text.alignment = TextAnchor.MiddleCenter;
		text.horizontalOverflow = HorizontalWrapMode.Overflow;
		text.verticalOverflow = VerticalWrapMode.Overflow;
		text.color = textColor;
		text.fontSize = fontSize;

		var rt = text.rectTransform;
		rt.anchorMax = new Vector2(0.5f, 0.5f);
		rt.anchorMin = new Vector2(0.5f, 0.5f);
		rt.anchoredPosition = Vector2.zero;

		return go;
	}

	public static GameObject Canvas(string name = "Canvas", RenderMode mode = RenderMode.ScreenSpaceOverlay)
	{
		GameObject go = new GameObject(name);
		AttachCanvas(go, mode);
		return go;
	}

	public static void AttachCanvas(GameObject go, RenderMode mode = RenderMode.ScreenSpaceOverlay)
	{
		if (go.GetComponent<Canvas>() != null)
		{
			return;
		}
		Canvas canvas = go.AddComponent<Canvas>();
		go.AddComponent<CanvasScaler>();
		go.AddComponent<GraphicRaycaster>();
		

		canvas.pixelPerfect = true;
		canvas.renderMode = mode;
	}

	static void AddEventSystem()
	{
		if (eventSystem == null)
		{
			GameObject go = new GameObject("Event System");
			eventSystem = go.AddComponent<EventSystem>();
			go.AddComponent<BaseInput>();
		}
	}

}



