﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TooltipTrigger : EventTrigger {
	static GameObject tooltipCanvas;

	string tooltipText = "";
	int fontSize = 18;
	GameObject go;

	public void SetText(string text) { tooltipText = text; }

	public override void OnPointerEnter(PointerEventData data)
	{
		Enter();
	}

	public override void OnPointerExit(PointerEventData data)
	{
		Exit();
	}

	public void Enter()
	{
		StartTooltip();
	}

	private void Exit()
	{
		Destroy(go);
	}

	void StartTooltip()
	{
		print("enabled " + enabled);
		if (!enabled)
		{
			return;
		}
		UIUtility.Fill = new Color(0.9f, 0.9f, 1f);
		UIUtility.Stroke = new Color(0.9f, 0.9f, 1f, 0.5f);
		UIUtility.StrokeWidth = 2;
		UIUtility.FontSize = fontSize;
		UIUtility.Padding = new RectOffset(5, 5, 3, 3);
		UIUtility.TextColor = Color.black;

		Rect rect = new Rect(new Vector2(0,0), new Vector2(0,0));
		go = UIUtility.Box(rect, transform, tooltipText);

		AttachToCanvas();//now the box is attached
		var fitter = go.AddComponent<ContentSizeFitter>();
		fitter.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
		fitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;

		go.AddComponent<Tooltip>();
	}

	void AttachToCanvas()
	{
		if (tooltipCanvas == null)
		{
			tooltipCanvas = new GameObject("Tooltip Canvas");
			var canvas = tooltipCanvas.AddComponent<Canvas>();
			canvas.renderMode = RenderMode.ScreenSpaceOverlay;
		}
		go.transform.SetParent(tooltipCanvas.transform);
	}

}
