﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tooltip : MonoBehaviour {
	float startTime;
	public static float delay = 0.7f;
	Image box;
	Text text;
	bool active = false;
	private void Start()
	{
		startTime = Time.time;
		box = transform.GetComponent<Image>();
		text = transform.GetChild(0).GetComponent<Text>();

		box.enabled = false;
		text.enabled = false;
	}

	private void Update()
	{
		if (!active && Time.time > startTime+delay)
		{
			active = true;
			box.enabled = true;
			text.enabled = true;
			enabled = false;
			Canvas.ForceUpdateCanvases();
			SetAnchor();
		}

	}

	void SetAnchor()
	{
		Canvas.ForceUpdateCanvases();
		var mousePos = Input.mousePosition;
		var size = box.rectTransform.rect.size;
		var window = new Vector2(Screen.width, Screen.height);
		var pointerZone = new Vector2(10, 15);

		box.rectTransform.anchorMin = new Vector2(1f, 0f);
		box.rectTransform.anchorMax = new Vector2(1f, 0f);

		var posOffset = new Vector2(0,0);

		//A) Top Right
		//B) Down left
		//C) Top
		//D) Right

		//D) Right
		if (mousePos.x > window.x - size.x)
		{
			if (mousePos.y > window.y - size.y)
			{//A) Top Right
			 //tooltip goes down left
				posOffset.x = -size.x / 2f - pointerZone.x;
				posOffset.y = -size.y / 2f - pointerZone.y*3.25f;
			}
			else
			{//D) Other Right
			 //tooltip goes up left
				posOffset.x = -size.x / 2f - pointerZone.x;
				posOffset.y = size.y / 2f + pointerZone.y;
			}
		}
		else//not right
		{
			if (mousePos.y > window.y - size.y)
			{//C) Top
			 //tooltip goes down right
				posOffset.x = +size.x / 2f + pointerZone.x*2f;
				posOffset.y = -size.y / 2f - pointerZone.y*3.25f;
			}
			else
			{//B) Down left
			 //tooltip goes up right
				posOffset.x = size.x / 2f + pointerZone.x*2f;
				posOffset.y = size.y / 2f + pointerZone.y;
			}
		}
		//posOffset = new Vector2(0,posOffset.y);
		var pos = new Vector2(Input.mousePosition.x + posOffset.x, Input.mousePosition.y + posOffset.y );
		box.rectTransform.position = pos;
	}
	
}
