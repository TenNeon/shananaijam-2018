﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class WorldUtility : MonoBehaviour {

    public static GameObject CheckForHit()
    {
        var point = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        return CheckForHit(point);
    }

    public static GameObject CheckForHit(Vector2 point)
    {
        Collider2D hit = Physics2D.OverlapPoint(point);

        if (hit != null)
        {
            var go = hit.transform.gameObject;
            if (go != null)
            {
                return go;
            }
        }
        return null;
    }

    public static bool NotOverUI()
    {
        return !EventSystem.current.IsPointerOverGameObject();
    }
}
