﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyBindingMenu : MonoBehaviour {
	static GameObject canvasObject;
	static GameObject menuToggleButtonObject;
	static GameObject menuScreenObject;
	static GameObject scrollContentObject;
	static string currentActionName = "";

	public static void Init()
	{
		canvasObject = new GameObject("Keybinding Menu Canvas");
		menuScreenObject = new GameObject("Keybinding Menu");
		menuScreenObject.transform.SetParent(canvasObject.transform);
		menuScreenObject.SetActive(false);

		SetupCanvas();
		SetupMenu();
		PopulateMenu();
		SetupButton();
	}

	static void SetupCanvas()
	{
		var canvas = canvasObject.AddComponent<Canvas>();
		canvasObject.AddComponent<CanvasScaler>();
		canvasObject.AddComponent<GraphicRaycaster>();
		canvas.renderMode = RenderMode.ScreenSpaceOverlay;
		canvas.pixelPerfect = true;
		canvas.sortingOrder = 1000;
	}

	static void SetupMenu()
	{
		int menuWidth = 600;
		int menuHeight = Screen.height-10;
		int padding = 5;
		int scrollViewportWidth = (menuWidth - padding * 2);
		int scrollContentWidth = scrollViewportWidth - padding*2;
		int rowWidth = (menuWidth - padding * 2)/2 - (padding*2) + padding/2;
		int rowHeight = 60;
		int headerHeight = 120;

		int rowCount = KeyManager.bindings.Count;
		List<GameObject> rows = new List<GameObject>();

		var menuBG = menuScreenObject.AddComponent<Image>();
		menuBG.color = ColorKeeper.GetColor("MenuColorBG");
		var menuRect = menuBG.rectTransform;
		menuRect.anchorMin = new Vector2(0, 1f);
		menuRect.anchorMax = new Vector2(0, 1f);
		menuRect.sizeDelta = new Vector2(menuWidth, menuHeight);
		menuRect.anchoredPosition = new Vector2(menuWidth/2, -menuHeight/2);

		//header
		GameObject headerRow = new GameObject("Header");
		headerRow.transform.SetParent(menuRect.transform);
		var hrRect = headerRow.AddComponent<RectTransform>();
		headerRow.AddComponent<HorizontalLayoutGroup>();

		SetRectTransform(hrRect, new Vector2(menuWidth, rowHeight), new Vector2(menuWidth/2f, -rowHeight*1.5f+padding), new Vector2(0,1), padding);

		var box = AddTextbox(new Vector2(rowWidth, rowHeight), new Vector2((rowWidth/2f), -(85)), new Vector2(0, 1), padding, headerRow.transform, "Action");
		var box2 = AddTextbox(new Vector2(rowWidth, rowHeight), new Vector2((rowWidth / 2f) + rowWidth + 18, -(85)), new Vector2(0, 1), padding, headerRow.transform, "Key");

		box.GetComponent<Text>().color = Color.white;
		box2.GetComponent<Text>().color = Color.white;

		var textBoxTransform = box.GetComponent<Text>().rectTransform;		

		//scrolling section
		var scrollViewportObject = new GameObject("Scroll Viewport");
		scrollViewportObject.transform.SetParent(menuScreenObject.transform);
		scrollViewportObject.AddComponent<RectMask2D>();
		var scrollRect = scrollViewportObject.AddComponent<ScrollRect>();
		scrollRect.inertia = false;
		scrollRect.movementType = ScrollRect.MovementType.Clamped;

		var scrollViewportRect = scrollViewportObject.GetComponent<RectTransform>();
		var scrollViewportAreaImage = scrollViewportObject.AddComponent<Image>();
		scrollViewportAreaImage.color = ColorKeeper.GetColor("MenuColorFG");
		scrollViewportRect.anchorMin = new Vector2(0, 1f);
		scrollViewportRect.anchorMax = new Vector2(0, 1f);
		scrollViewportRect.sizeDelta = new Vector2(scrollViewportWidth, menuHeight - headerHeight);
		scrollViewportRect.anchoredPosition = new Vector2(menuWidth / 2, (-menuHeight / 2)-headerHeight/2+padding );

		scrollContentObject = new GameObject("Scroll Content");
		scrollContentObject.transform.SetParent(scrollViewportObject.transform);
		//size content to viewport width, and content height
		var scrollContentRect = scrollContentObject.AddComponent<RectTransform>();
		scrollContentRect.anchorMin = new Vector2(0, 1f);
		scrollContentRect.anchorMax = new Vector2(0, 1f);

		var scrollContentHeight = (rowHeight + padding) * rowCount;
		scrollContentRect.sizeDelta = new Vector2(scrollContentWidth, scrollContentHeight);
		scrollContentRect.anchoredPosition = new Vector2(menuWidth/2 - padding, -scrollContentHeight/2);

		var layout = scrollContentObject.AddComponent<VerticalLayoutGroup>();
		layout.padding = new RectOffset(0, 0, padding, padding);
		layout.spacing = padding;
		layout.childControlWidth = false;
		layout.childForceExpandWidth = false;

		for (int i = 0; i < rowCount; i++)
		{
			var row = AddRow(new Vector2(rowWidth, rowHeight), padding);

			row.transform.SetParent(scrollContentObject.transform);
			rows.Add(row);
		}

		scrollRect.content = scrollContentRect;
		scrollRect.viewport = scrollViewportRect;
		scrollRect.inertia = false;
		scrollRect.horizontal = false;
		scrollRect.scrollSensitivity = rowHeight+padding;

	}

	static GameObject AddRow(Vector2 size, int padding)
	{
		GameObject newRow = new GameObject("Row");
		GameObject leftColumn = new GameObject("Left column");
		GameObject rightColumn = new GameObject("Right column");

		leftColumn.transform.SetParent(newRow.transform);
		rightColumn.transform.SetParent(newRow.transform);

		var layout = newRow.AddComponent<HorizontalLayoutGroup>();
		//layout.padding = new RectOffset(0, 0, 0 , 0);
		layout.spacing = padding;
		layout.childControlWidth = false;
		layout.childForceExpandWidth = false;

		var leftImage = leftColumn.AddComponent<Image>();
		var rightImage = rightColumn.AddComponent<Image>();
		var leftRect = leftImage.rectTransform;
		var rightRect = rightImage.rectTransform;

		leftImage.color = ColorKeeper.GetColor("MenuColorItem");
		rightImage.color = ColorKeeper.GetColor("MenuColorItem");

		leftRect.anchorMin = new Vector2(0, 1f);
		leftRect.anchorMax = new Vector2(0, 1f);
		leftRect.sizeDelta = new Vector2(size.x, size.y);

		rightRect.anchorMin = new Vector2(0, 1f);
		rightRect.anchorMax = new Vector2(0, 1f);
		rightRect.sizeDelta = new Vector2(size.x, size.y);

		var position = new Vector2(size.x/2f, size.y/2f);
		AddTextbox(size, position, Vector2.zero, padding, leftColumn.transform, "TEXT BOX");

		return newRow;
	}

	static GameObject AddDropdown(RectTransform rt, Transform parent)
	{
		var go = Instantiate(PrefabKeeper.prefabs["keybindDropdown"]);
		go.transform.SetParent(parent);

		go.GetComponent<Image>().color = ColorKeeper.GetColor("MenuColorItem");

		var goRect = go.GetComponent<RectTransform>();

		var pos = new Vector2(rt.sizeDelta.x/2f, rt.sizeDelta.y/2f);
		SetRectTransform(goRect, rt.sizeDelta, pos, Vector2.zero, 0);

		go.transform.GetChild(0).GetComponent<Text>();

		//var arrowRect = go.transform.GetChild(1).GetComponent<RectTransform>();
		var templateRect = go.transform.GetChild(1).GetComponent<RectTransform>();

		var size = new Vector2(rt.sizeDelta.x, rt.sizeDelta.y*4f);
		var templatePos = new Vector2(pos.x, pos.y - rt.sizeDelta.y / 2f);
		SetRectTransform(templateRect, size, templatePos, Vector2.zero, 0);


		var contentRect = go.transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<RectTransform>();
		var contentSize = contentRect.sizeDelta;
		contentSize.y = rt.sizeDelta.y;
		contentRect.sizeDelta = contentSize;


		var itemRect = contentRect.transform.GetChild(0).GetComponent<RectTransform>();
		var itemSize = itemRect.sizeDelta;
		itemSize.y = rt.sizeDelta.y;
		itemRect.sizeDelta = itemSize;

		return go;
	}

	static GameObject AddButton(RectTransform rt, Transform parent)
	{
		var go = Instantiate(PrefabKeeper.prefabs["button"]);
		go.transform.SetParent(parent);
		var goRect = go.GetComponent<RectTransform>();
		go.GetComponent<Image>().color = ColorKeeper.GetColor("MenuColorItem");
		var pos = new Vector2(rt.sizeDelta.x / 2f, rt.sizeDelta.y / 2f);
		SetRectTransform(goRect, rt.sizeDelta, pos, Vector2.zero, 0);

		return go;
	}

	static GameObject AddButton(Vector2 size, Vector2 position, int padding, Transform parent)
	{
		GameObject buttonObject = new GameObject("Button");
		buttonObject.transform.SetParent(parent);
		var buttonRect = buttonObject.AddComponent<RectTransform>();
		buttonObject.AddComponent<Button>();

		SetRectTransform(buttonRect, size, position, Vector2.zero, padding);

		return buttonObject;
	}

	static GameObject AddModificationButtons(Transform parent, string keybindName)
	{
		var go = Instantiate(PrefabKeeper.prefabs["addRemoveKeybind"]);
		go.transform.SetParent(parent);
		var rt = go.GetComponent<RectTransform>();
		SetRectTransform(rt, new Vector2(24f,48f), new Vector2(-12f,0), new Vector2(1f,0.5f), 0);

		//attach actions to buttons
		var addButton = go.transform.GetChild(0).GetComponent<Button>();
		var removeButton = go.transform.GetChild(1).GetComponent<Button>();

		addButton.onClick.AddListener(delegate { CaptureKeyCombo(keybindName); } );
		

		removeButton.onClick.AddListener(delegate { RemoveFromKeybind(keybindName); });


		return go;
	}

	static GameObject AddTextbox(Vector2 size, Vector2 position, Vector2 anchor, int padding, Transform parent, string initialText = "")
	{
		GameObject textBoxObject = AddTextbox("Text Box");
		textBoxObject.transform.SetParent(parent);

		Text text = textBoxObject.GetComponent<Text>();
		RectTransform textRect = text.rectTransform;
		text.text = initialText;
		text.fontSize = 42;

		SetRectTransform(textRect, size, position, anchor, padding);

		return textBoxObject;
	}

	static void SetupButton()
	{
		int boxWidth = 170;
		int boxHeight = 60;
		int padding = 5;
		menuToggleButtonObject = new GameObject("Menu Toggle Button");
		menuToggleButtonObject.transform.SetParent(canvasObject.transform);
		var toggleRectTransform = menuToggleButtonObject.AddComponent<RectTransform>();
		toggleRectTransform.anchorMin = new Vector2(0,1f);
		toggleRectTransform.anchorMax = new Vector2(0,1f);
		toggleRectTransform.sizeDelta = new Vector2(boxWidth, boxHeight);//sure why not
		toggleRectTransform.anchoredPosition = new Vector2(boxWidth/2f, -boxHeight/2f);

		var toggle = menuToggleButtonObject.AddComponent<Toggle>();
		toggle.onValueChanged.AddListener(delegate { ToggleMenu(); });

		var menuToggleImage = menuToggleButtonObject.AddComponent<Image>();
		menuToggleImage.color = new Color(1.0f, 1.0f, 1.0f);
		toggle.image = menuToggleImage;

		var colors = toggle.colors;
		colors.normalColor = ColorKeeper.GetColor("ButtonColorNormal");
		colors.highlightedColor = ColorKeeper.GetColor("ButtonColorHighlight");
		colors.pressedColor = ColorKeeper.GetColor("ButtonColorPressed");
		toggle.colors = colors;
		
		var menuToggleCheckMark = new GameObject("Checkmark");
		menuToggleCheckMark.transform.SetParent(menuToggleButtonObject.transform);
		var checkMarkImage = menuToggleCheckMark.AddComponent<Image>();
		checkMarkImage.color = new Color(1f, 1f, 1f, 0.5f);
		var checkMarkImageRectTransform = checkMarkImage.rectTransform;
		checkMarkImageRectTransform.anchorMin = new Vector2(0, 1f);
		checkMarkImageRectTransform.anchorMax = new Vector2(0, 1f);
		checkMarkImageRectTransform.sizeDelta = new Vector2(boxWidth-4, boxHeight-4);
		checkMarkImageRectTransform.anchoredPosition = new Vector2(boxWidth / 2f, -boxHeight / 2f);

		GameObject textObject = new GameObject("Text");
		textObject.transform.SetParent(menuToggleButtonObject.transform);
		var text = textObject.AddComponent<Text>();
		text.text = "Key Bindings";//12 characters x 12pt = 144px
		text.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
		text.alignment = TextAnchor.MiddleCenter;
		text.fontSize = 24;

		var textRectTransform = text.rectTransform;
		textRectTransform.anchorMin = Vector2.zero;
		textRectTransform.anchorMax = Vector2.zero;

		float textBoxWidth = boxWidth - (padding * 2);
		float textboxHeight =  boxHeight - (padding * 2);
		textRectTransform.sizeDelta = new Vector2(textBoxWidth, textboxHeight);
		textRectTransform.anchoredPosition = new Vector2(boxWidth / 2f, boxHeight / 2f);

		toggle.graphic = checkMarkImage;
	}

	static void PopulateMenu()
	{
		var bindings = KeyManager.bindings;

		var bindingNames = new List<string>(bindings.Keys);
		//var bindingValues = new List<Keybind>(bindings.Values);
		int i = 0;
		foreach (Transform child in scrollContentObject.transform)
		{
			PopulateKeybindRow(child.transform, bindingNames[i]);
			i++;
		}
	}

	static void PopulateKeybindRow(Transform parent, string keybindName)
	{
		parent.gameObject.GetComponent<RectTransform>();
		var keybind = KeyManager.bindings[keybindName];

		var leftColumn = parent.GetChild(0);
		var rightColumn = parent.GetChild(1);

		var leftRT = leftColumn.gameObject.GetComponent<RectTransform>();
		var rightRT = rightColumn.gameObject.GetComponent<RectTransform>();
		
		//left column stuff
		var text = leftColumn.GetChild(0).GetComponent<Text>();

		text.text = keybindName;
		//rename the row
		parent.gameObject.name = keybindName;

		//right column stuff
		//clear the row
		foreach (Transform child in rightColumn)
		{
			Destroy(child.gameObject);
		}

		if (keybind.keyCombos.Count > 1)
		{
			// do a dropdown
			var dropdown = AddDropdown(rightRT, rightColumn).GetComponent<Dropdown>();
			AddModificationButtons(rightColumn, keybindName);
			AttachKeybind(dropdown, keybind);
		}
		else
		{
			//do a button
			var button = AddButton(leftRT, rightColumn).GetComponent<Button>();
			AddModificationButtons(rightColumn, keybindName);
			AttachKeybind(button, keybind);
		}
	}

	static void AttachKeybind(Dropdown dd, Keybind keybind)
	{
		var options = new List<Dropdown.OptionData>();
		for (int j = 0; j < keybind.keyCombos.Count; j++)
		{
			options.Add(new Dropdown.OptionData(keybind.keyCombos[j].ToString()));
		}

		dd.options = options;
	}

	static void AttachKeybind(Button button, Keybind keybind)
	{
		var text = button.transform.GetChild(0).GetComponent<Text>();

		if (keybind.keyCombos.Count > 0)
		{
			text.text = keybind.keyCombos[0].ToString();
		}
		else
		{
			text.text = "unbound";
		}
		
	}

	static void SetRectTransform(RectTransform rt, Vector2 size, Vector2 position, Vector2 anchor, int padding = 0)
	{
		rt.anchorMin = anchor;
		rt.anchorMax = anchor;
		var newSize = new Vector2(size.x-(padding*2f), size.y-(padding*2f));
		rt.sizeDelta = newSize;
		rt.anchoredPosition = position;
	}

	static void ToggleMenu()
	{
		menuScreenObject.SetActive(!menuScreenObject.activeSelf);
	}

	static GameObject AddTextbox(string objectName)
	{
		GameObject textboxObject = new GameObject(objectName);
		AddTextbox(textboxObject, objectName);
		var text = textboxObject.GetComponent<Text>();
		text.text = objectName;
		text.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
		text.alignment = TextAnchor.MiddleCenter;
		text.fontSize = 14;
		text.color = Color.black;

		return textboxObject;
	}

	static Text AddTextbox(GameObject owner, string initialText)
	{
		var text = owner.AddComponent<Text>();
		text.text = initialText;
		text.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
		text.alignment = TextAnchor.MiddleCenter;
		text.fontSize = 14;
		text.color = Color.black;

		return text;
	}

	static void ReplaceKeybind(string keybindToReplace, string newKeybindName, Keybind newKeybind)
	{

	}

	//remove the currently selected key combo from the named keybind
	static void RemoveFromKeybind(string keybindToRemoveFrom)
	{
		if (!KeyManager.bindings.ContainsKey(keybindToRemoveFrom))
		{
			return;
		}

		Transform currentRow = GetRow(keybindToRemoveFrom);

		if (currentRow == null)
		{
			return;
		}

		//if there is only one bound combo, remove it
		if (KeyManager.bindings[keybindToRemoveFrom].keyCombos.Count == 1)
		{
			KeyManager.bindings[keybindToRemoveFrom].keyCombos.RemoveAt(0);
		}
		else
		{
			//there are multiple combos for this keybind
			//get the currently selected combo for this keybind
			var rightColumn = currentRow.GetChild(1);
			var dd = rightColumn.GetChild(0).gameObject.GetComponent<Dropdown>();
			if (dd.value < KeyManager.bindings[keybindToRemoveFrom].keyCombos.Count)
			{
				KeyManager.bindings[keybindToRemoveFrom].keyCombos.RemoveAt(dd.value);
			}
		}

		//refresh the row
		PopulateKeybindRow(currentRow, keybindToRemoveFrom);
	}

	static Transform GetRow(string rowName)
	{
		Transform foundRow = null;
		foreach (Transform row in scrollContentObject.transform)
		{
			if (row.name == rowName)
			{
				foundRow = row;
				break;
			}
		}
		return foundRow;
	}

	static void Hide(GameObject go)
	{

	}

	static void Show(GameObject go)
	{

	}

	public static void CaptureKeyCombo(string actionName)
	{
		//capturing a key combo
		//keys are pressed
		//a key is released
		//record all the keys that were pressed when the key was released (plus that key)
		currentActionName = actionName;
		GameObject keyCaptureObject = new GameObject("Key Capture Object");
		keyCaptureObject.AddComponent<KeyCapture>();

		Transform currentRow = GetRow(currentActionName);
		var img = currentRow.GetChild(1).GetChild(0).GetComponent<Image>();
		img.color = ColorKeeper.colors["HighlightColor"];

		var text = img.gameObject.transform.GetChild(0).GetComponent<Text>();
		text.text = "________";
	}

	public static void KeyComboReleased(List<Key> keysUp)
	{
		if (!KeyManager.bindings.ContainsKey(currentActionName))
		{
			return;
		}

		var pressedKeys = KeyManager.GetKeysPressed();

		for (int i = 0; i < keysUp.Count; i++)
		{
			if (!pressedKeys.Contains(keysUp[i]))
			{
				pressedKeys.Add(keysUp[i]);
			}
		}

		KeyManager.AddKeybind(currentActionName, pressedKeys);

		Transform currentRow = GetRow(currentActionName);
		if (currentRow != null)
		{
			PopulateKeybindRow(currentRow, currentActionName);
		}
		

		currentActionName = "";
	}
}
