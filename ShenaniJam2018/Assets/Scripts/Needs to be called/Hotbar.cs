﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hotbar
{
	public delegate void BoxSelection(string newSelection);
	public static event BoxSelection BoxSelectionEvent;

	static GameObject panelObject;
	static GameObject canvasObject;
	static GameObject tabBarObject;

	static List<List<BoxInfo>> tabs;
	static List<GameObject> tabIndicators;
	static List<GameObject> hotbar;
	
	public static readonly int boxCount = 10;
	public static readonly int boxWidth = 64;
	public static readonly int boxHeight = 64;
	public static readonly int boxSpacing = 8;

	public static readonly int tabCount = 7;
	public static readonly int tabWidth = 64;
	public static readonly int tabHeight = 20;
	public static readonly int tabSpacing = 8;

	static int tabIndex = 0;
	static int selectedBox = -1;

	public static int TabIndex { get { return tabIndex; } }

	public static void Init()
	{
		canvasObject = new GameObject("Hotbar Canvas");
		panelObject = new GameObject("Hotbar Panel");
		tabBarObject = new GameObject("Hotbar Tabs");
		hotbar = new List<GameObject>();

		PopulateTabs();
		SetupCanvas();
		SetupPanel();
		SetupTabBar();
		SetupBoxes();
		SetupTabs();
		SetSpecialBoxes();

		UpdateTab();
		UpdateBoxes();
	}

	private static void PopulateTabs()
	{
		if (tabs == null)
		{
			tabs = new List<List<BoxInfo>>();
		}

		var randomSprites = SpriteList.main.GetShuffledSpriteList();

		for (int i = 0; i < tabCount; i++)
		{
			List<BoxInfo> tab = new List<BoxInfo>();
			for (int j = 0; j < boxCount; j++)
			{
				BoxInfo newBoxInfo = new BoxInfo();

				if (randomSprites == null || i * boxCount + j >= randomSprites.Count)
				{

					newBoxInfo.info = "empty";
					newBoxInfo.spriteName = "";
				}
				else
				{
					var name = randomSprites[i * boxCount + j].name;
					newBoxInfo.info = "paint action";
					newBoxInfo.spriteName = name;
				}
				tab.Add(newBoxInfo);
			}

			tabs.Add(tab);
		}
	}

	static void SetupCanvas()
	{
		var canvas = canvasObject.AddComponent<Canvas>();
		canvasObject.AddComponent<CanvasScaler>();
		canvasObject.AddComponent<GraphicRaycaster>();
		canvas.renderMode = RenderMode.ScreenSpaceOverlay;
		canvas.pixelPerfect = true;
	}

	static void SetupPanel()
	{
		panelObject.transform.SetParent(canvasObject.transform);

		var layout = panelObject.AddComponent<HorizontalLayoutGroup>();
		var toggleGroup = panelObject.AddComponent<ToggleGroup>();
		toggleGroup.allowSwitchOff = true;

		layout.spacing = boxSpacing;
		layout.padding = new RectOffset(boxSpacing, boxSpacing, boxSpacing, boxSpacing);
		layout.childControlWidth = false;
		layout.childForceExpandWidth = false;

		var panelImage = panelObject.AddComponent<Image>();
		panelImage.color = Color.white;

		var panelRect = panelObject.GetComponent<RectTransform>();
		var w = boxCount * boxWidth + ((boxSpacing * (boxCount-1))) + boxSpacing * 2;
		var h = boxHeight + boxSpacing * 2;
		panelRect.sizeDelta = new Vector2(w, h);
		panelRect.anchorMin = new Vector2(0.5f, 0);
		panelRect.anchorMax = new Vector2(0.5f, 0);
		panelRect.anchoredPosition = new Vector2(0, Mathf.RoundToInt( h/2 ));
	}

	private static void SetupTabBar()
	{
		tabBarObject.transform.SetParent(canvasObject.transform);
		var layout = tabBarObject.AddComponent<HorizontalLayoutGroup>();

		layout.spacing = tabSpacing;
		layout.padding = new RectOffset(tabSpacing, tabSpacing, tabSpacing, tabSpacing);
		var tabBarImage = tabBarObject.AddComponent<Image>();
		tabBarImage.color = Color.white;

		var tabBarRect = tabBarObject.GetComponent<RectTransform>();
		var w = tabCount * tabWidth + ((tabSpacing * (tabCount - 1))) + tabSpacing * 2;
		var h = tabHeight + tabSpacing * 2;
		tabBarRect.sizeDelta = new Vector2(w, h);
		tabBarRect.anchorMin = new Vector2(0.5f, 0);
		tabBarRect.anchorMax = new Vector2(0.5f, 0);

		var panelRect = panelObject.GetComponent<RectTransform>();

		var xPos = -(panelRect.rect.width / 2 - tabBarRect.rect.width / 2);

		tabBarRect.anchoredPosition = new Vector2(xPos, panelRect.rect.height + Mathf.RoundToInt(h / 2));
	}

	static void SetupBoxes()
	{
		var toggleGroup = panelObject.GetComponent<ToggleGroup>() as ToggleGroup;
		for (int i = 0; i < boxCount; i++)
		{
			AddBox(i, toggleGroup);
		}
	}

	static void SetupTabs()
	{
		tabIndicators = new List<GameObject>();
		for (int i = 0; i < tabCount; i++)
		{
			AddTab(i);
		}
		UpdateActiveTab();
	}

	static void SetSpecialBoxes()
	{

	}

	static void AddBox(int value, ToggleGroup tg)
	{
		GameObject newBox = new GameObject("Box " + value);
		GameObject imageObject = new GameObject("Image");
		GameObject bgObject = new GameObject("Background");
		GameObject checkmarkObject = new GameObject("Checkmark");

		newBox.transform.SetParent(panelObject.transform);
		bgObject.transform.SetParent(newBox.transform);
		imageObject.transform.SetParent(bgObject.transform);
		checkmarkObject.transform.SetParent(imageObject.transform);

		var boxRectTransform = newBox.AddComponent<RectTransform>();
		boxRectTransform.anchorMin = Vector2.zero;
		boxRectTransform.anchorMax = Vector2.zero;
		boxRectTransform.sizeDelta = new Vector2(boxWidth, boxHeight);

		int imgWidth = 48;
		var boxImage = imageObject.AddComponent<Image>();
		boxImage.rectTransform.anchorMin = new Vector2(0.5f, 0.5f);
		boxImage.rectTransform.anchorMax = new Vector2(0.5f, 0.5f);
		boxImage.color = Color.white;
		boxImage.rectTransform.sizeDelta = new Vector2(imgWidth, imgWidth);

		var backgroundImage = bgObject.AddComponent<Image>();
		backgroundImage.rectTransform.anchorMin = new Vector2(0.5f, 0.5f);
		backgroundImage.rectTransform.anchorMax = new Vector2(0.5f, 0.5f);
		backgroundImage.color = new Color(.8f,.95f,1f, 1f);
		backgroundImage.rectTransform.sizeDelta = new Vector2(boxWidth, boxWidth);

		var checkboxImage = checkmarkObject.AddComponent<Image>();
		checkboxImage.rectTransform.anchorMin = new Vector2(0.5f, 0.5f);
		checkboxImage.rectTransform.anchorMax = new Vector2(0.5f, 0.5f);
		checkboxImage.color = new Color(1f,0.5f,0);
		int checkboxWidth = 64;
		checkboxImage.rectTransform.sizeDelta = new Vector2(checkboxWidth, checkboxWidth);
		checkboxImage.sprite = SpriteList.main.GetSprite("Box Outline");

		//image goes toggle->bg->checkmark
		var toggle = newBox.AddComponent<Toggle>();
		toggle.group = tg;
		toggle.targetGraphic = backgroundImage;
		toggle.graphic = checkboxImage;
		toggle.onValueChanged.AddListener(delegate { SelectBox(value); });

		var colors = toggle.colors;

		colors.normalColor = new Color(0.7f, 0.7f, 0.6f);
		colors.highlightedColor = new Color(1f, 1f, 1f);
		colors.pressedColor = new Color(0.5f, 0.5f, 0.5f);
		toggle.colors = colors;

		var ttt = newBox.AddComponent<TooltipTrigger>();
		ttt.enabled = false;

		hotbar.Add(newBox);
	}

	static void AddTab(int value)
	{
		GameObject newTab = new GameObject("Tab " + value);
		newTab.transform.SetParent(tabBarObject.transform);
		var boxImage = newTab.AddComponent<Image>();
		boxImage.rectTransform.anchorMin = Vector2.zero;
		boxImage.rectTransform.anchorMax = Vector2.zero;
		boxImage.color = new Color(0.9f, 0.9f, 0.9f);

		tabIndicators.Add(newTab);
	}

	public static void SetTab(List<int> spriteIndices)
	{

	}

	//set all tabs inactive but this one
	static void UpdateActiveTab()
	{
		if (tabIndicators == null)
		{
			return;
		}
		for (int i = 0; i < tabIndicators.Count; i++)
		{
			var img = tabIndicators[i].GetComponent<Image>();

			if (i == tabIndex)
			{
				img.color = Color.yellow;
			}
			else
			{
				img.color = new Color(0.9f, 0.9f, 0.9f);
			}
		}
	}

	static void UpdateBoxes()
	{
		if (hotbar == null)
		{
			Debug.Log("no hotbar");
			return;
		}
		if (tabs == null)
		{
			Debug.Log("no tabs");
			return;
		}

		List<BoxInfo> thisTab = tabs[tabIndex];
		for (int i = 0;  i < thisTab.Count;  i++)
		{
			//image goes toggle->bg->Image->checkmark
			var box = hotbar[i].transform;
			var toggle = box.GetComponent<Toggle>() as Toggle;
			var img = box.GetChild(0).GetChild(0).GetComponent<Image>();
			var tooltip = box.GetComponent<TooltipTrigger>();
			tooltip.SetText(thisTab[i].spriteName);

			img.sprite = SpriteList.main.GetSprite(thisTab[i].spriteName);

			//if the sprite is null, disable the toggle
			if (img.sprite == null)
			{
				toggle.interactable = false;
				tooltip.enabled = false;
			}
			else
			{
				toggle.interactable = true;
				tooltip.enabled = true;
			}
		}
	}

	public static int NextTab()
	{
		tabIndex += 1;

		if (tabIndex >= tabCount)
		{
			tabIndex = 0;
		}
		UpdateTab();
		return tabIndex;
	}

	public static int PreviousTab()
	{
		tabIndex -= 1;

		if (tabIndex < 0)
		{
			tabIndex = tabCount - 1;
		}
		UpdateTab();
		return tabIndex;
	}

	static void UpdateTab()
	{
		//update the active tab indicator
		UpdateActiveTab();

		//go through all boxes and put the correct image into them
		UpdateBoxes();

		//update the selected thing
		SelectBoxContents();

	}

	public static void ToggleBoxSelection(int boxIndex)
	{
		if (boxIndex < 0 || boxIndex >= hotbar.Count)
		{
			return;
		}
		//find the toggle fo boxIndex
		//click the toggle for boxIndex
		Toggle toggle = hotbar[boxIndex].GetComponent<Toggle>() as Toggle;
		toggle.isOn = !toggle.isOn;
	}

	public static void ToggleTabSelection(int newTabIndex)
	{
		if (newTabIndex < 0 || newTabIndex >= tabCount)
		{
			return;
		}
		tabIndex = newTabIndex;
		UpdateTab();
	}

	public static void SelectBox(int boxIndex)
	{
		if (selectedBox == boxIndex)
		{
			selectedBox = -1;
		}
		else
		{
			selectedBox = boxIndex;
			SelectBoxContents();
		}
	}

	public static void SetBox(BoxInfo newInfo, int newTabIndex, int newBoxIndex)
	{
		if (newTabIndex >=0 && newTabIndex < tabs.Count && newBoxIndex >=0 && newBoxIndex < boxCount)
		{
			tabs[newTabIndex][newBoxIndex] = newInfo;
			UpdateBoxes();
		}
	}

	static void SelectBoxContents()
	{
		if (selectedBox < 0 || selectedBox >= boxCount)
		{
			return;
		}
		//send the contents of the currently selected box
		var boxInfo = tabs[tabIndex][selectedBox].info;
		if (BoxSelectionEvent != null)
		{
			if (boxInfo == "")
			{
				hotbar[selectedBox].GetComponent<Toggle>().isOn = false;
			}
			BoxSelectionEvent(boxInfo);
		}
	}

	public static BoxInfo GetSelectedBox()
	{
		if (tabIndex >= 0 && tabIndex < tabCount && selectedBox >= 0 && selectedBox < boxCount)
		{
			return tabs[tabIndex][selectedBox];
		}

		return new BoxInfo();
	}
}
