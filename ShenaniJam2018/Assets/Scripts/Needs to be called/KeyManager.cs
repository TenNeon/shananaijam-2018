﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyManager : MonoBehaviour {

	public static Dictionary<string, Keybind> bindings;
	public static Dictionary<string, Key> keys;

	static int[] keyCodes;
	public static int keysPressed;
	public static int keysLastTracked = 0;
	public delegate bool KeyCheckDelegate();

	enum KeyEventType {KeyDown, KeyPressed, KeyUp };

	public static void Init()
	{
		keys = new Dictionary<string, Key>();
		SetupKeyTracker();
		InitKeysFromKeyCodes();

		AddKeybind("rotate", keys["Space"]);
		AddKeybind("rotate", keys["Mouse0"]);

		//AddKeybind("Menu", new List<Key>() { keys["Escape"]});
	}

	private static void SetupKeyTracker()
	{
		keyCodes = (int[])System.Enum.GetValues(typeof(KeyCode));
	}

	static void InitKeysFromKeyCodes()
	{
		for (int i = 0; i < keyCodes.Length; i++)
		{
			string keyString = ((KeyCode)keyCodes[i]).ToString();
			KeyCode keyCode = (KeyCode)keyCodes[i];

			if (!keys.ContainsKey(keyString))
			{
				keys.Add(keyString, new ButtonKey(keyString, keyCode));
			}
		}
	}

	public static void UpdateTrackedKeys()
	{
		if (Time.frameCount == keysLastTracked)
		{
			return;
		}
		keysPressed = 0;
		for (int i = 0; i < keyCodes.Length; i++)
		{
			if (Input.GetKey((KeyCode)keyCodes[i]))
			{
				keysPressed++;
			}
		}

		if (Input.mouseScrollDelta.y != 0)
		{
			keysPressed++;
		}
		keysLastTracked = Time.frameCount;
	}

	public static List<Key> GetKeysPressed()
	{
		List<Key> foundKeys = new List<Key>();
		var keyNames = new List<string>(keys.Keys);

		for (int i = 0; i < keyNames.Count; i++)
		{
			if (keys[keyNames[i]].IsPressed())
			{
				foundKeys.Add(keys[keyNames[i]]);
			}
		}

		return foundKeys;
	}

	public static List<Key> GetKeysUp()
	{
		List<Key> foundKeys = new List<Key>();
		var keyNames = new List<string>(keys.Keys);

		for (int i = 0; i < keyNames.Count; i++)
		{
			if (keys[keyNames[i]].KeyUp())
			{
				foundKeys.Add(keys[keyNames[i]]);
			}
		}

		return foundKeys;
	}

	public static void AddKeybind(string name, Key key)
	{
		List<Key> keys = new List<Key>();
		keys.Add(key);
		AddKeybind(name, keys);
	}

	public static void AddKeybind(string name)
	{
		AddKeybind(name, new List<Key>());		
	}

	public static void AddKeybind(string name, List<Key> keys)
	{
		if (bindings == null)
		{
			bindings = new Dictionary<string, Keybind>();
		}

		if (!bindings.ContainsKey(name))
		{
			Keybind bind = new Keybind();

			bind.AddKey(keys);
			bindings.Add(name, bind);
		}
		else
		{
			Keybind bind = bindings[name];
			bind.AddKey(keys);
		}
	}

	public static bool GetKey(string keyName)
	{
		if (!Input.anyKey && (Input.mouseScrollDelta.y == 0))
		{
			return false;
		}
		
		return KeyCheck(keyName, KeyEventType.KeyPressed);

	}

	public static bool GetKeyDown(string keyName)
	{
		if (!Input.anyKeyDown && (Input.mouseScrollDelta.y == 0))
		{
			return false;
		}
		bool found = KeyCheck(keyName, KeyEventType.KeyDown);
		return found;
	}

	public static bool GetKeyUp(string keyName)
	{
		return KeyCheck(keyName, KeyEventType.KeyUp);
	}

	static bool KeyCheck(string keyName, KeyEventType type)
	{
		if (bindings.ContainsKey(keyName))
		{
			var keys = bindings[keyName].keyCombos;
			for (int i = 0; i < keys.Count; i++)
			{
				bool positiveMatch = false;
				switch (type)
				{
					case KeyEventType.KeyDown:
						positiveMatch = keys[i].KeyDown(); ;
						break;
					case KeyEventType.KeyPressed:
						positiveMatch = keys[i].IsPressed();
						break;
					case KeyEventType.KeyUp:
						positiveMatch = keys[i].KeyUp();
						break;
				}
				if (positiveMatch)
				{
					return true;
				}
			}
		}
		return false;
	}
}



public struct Keybind
{
	//public string name;
	public List<KeyCombo> keyCombos;

	public bool KeyDown()
	{
		for (int i = 0; i < keyCombos.Count; i++)
		{
			if (keyCombos[i].KeyDown())
			{
				return true;
			}
		}
		return false;
	}

	public bool KeyUp()
	{
		for (int i = 0; i < keyCombos.Count; i++)
		{
			if (keyCombos[i].KeyUp())
			{
				return true;
			}
		}
		return false;
	}

	public bool IsPressed()
	{
		for (int i = 0; i < keyCombos.Count; i++)
		{
			if (keyCombos[i].IsPressed())
			{
				return true;
			}
		}
		return false;
	}

	public void AddKey(List<Key> keys)
	{
		if (keyCombos == null)
		{
			keyCombos = new List<KeyCombo>();
		}

		if (keys.Count == 0)
		{
			return;
		}

		KeyCombo combo = new KeyCombo();
		combo.keys = new List<Key>();
		for (int i = 0; i < keys.Count; i++)
		{
			combo.keys.Add(keys[i]);
		}


		keyCombos.Add(combo);
	}

	bool HasCombo(KeyCombo combo)
	{
		for (int i = 0; i < keyCombos.Count; i++)
		{
			if (keyCombos[i].Matches(combo) )
			{
				return true;
			}
		}
		return false;
	}

	public string GetFirst()
	{
		return keyCombos[0].ToString();
	}
}

public struct KeyCombo
{
	public List<Key> keys;

	public bool IsPressed()
	{
		if (KeyManager.keysPressed != keys.Count)
		{
			return false;
		}

		for (int i = 0; i < keys.Count; i++)
		{
			if (!keys[i].IsPressed())
			{
				return false;
			}
		}
		return true;
	}

	public bool Matches(KeyCombo combo)
	{
		if (combo.keys.Count != keys.Count)
		{
			return false;
		}

		for (int i = 0; i < keys.Count; i++)
		{
			bool found = true;
			for (int j = 0; i < combo.keys.Count; j++)
			{
				if (keys[i] != combo.keys[j])
				{
					found = false;
				}
				else
				{
					found = true;
					break;
				}
			}
			if (!found)
			{
				return false;
			}
		}

		return true;
	}

	bool HasDown()
	{
		for (int i = 0; i < keys.Count; i++)
		{
			if (keys[i].KeyDown())
			{
				return true;
			}
		}
		return false;
	}

	bool HasUp()
	{
		for (int i = 0; i < keys.Count; i++)
		{
			if (keys[i].KeyUp())
			{
				return true;
			}
		}
		return false;
	}

	//the combo was completed this frame
	//all keys are pressed, and at least one is down
	public bool KeyDown()
	{
		bool hasdown = HasDown();
		bool pressed = IsPressed();
		return hasdown && pressed;
		//return (HasDown() && IsPressed());
	}

	public bool KeyUp()
	{
		return (HasUp() && !IsPressed());
	}

	public override string ToString()
	{
		string str = "";
		for (int i = 0; i < keys.Count; i++)
		{
			str += keys[i].name;
			if (i < keys.Count-1)
			{
				str += " + ";
			}
		}
		return str;
	}
}

public abstract class Key
{
	public string name;
	public abstract bool KeyUp(); 
	public abstract bool KeyDown(); 
	public abstract bool IsPressed(); 
}

public class ButtonKey : Key
{
	KeyCode code;

	public ButtonKey(string nameIn, KeyCode codeIn)
	{
		name = nameIn;
		code = codeIn;
	}

	public override bool IsPressed()
	{
		return Input.GetKey(code);
	}

	public override bool KeyDown()
	{
		return Input.GetKeyDown(code);
	}

	public override bool KeyUp()
	{
		return Input.GetKeyUp(code);
	}
}

public class ScrollKey : Key
{
	public enum Direction
	{
		Up,
		Down
	}

	Direction direction;

	public ScrollKey(Direction directionIn)
	{
		direction = directionIn;
		if (direction == Direction.Up)
		{
			name = "scrollwheel up";
		}
		else
		{
			name = "scrollwheel down";
		}
	}

	public override bool IsPressed()
	{
		return KeyDown();
	}

	public override bool KeyDown()
	{
		var delta = Input.mouseScrollDelta.y;
		return direction == Direction.Up ? delta > 0 : delta < 0;
	}

	public override bool KeyUp()
	{
		return KeyDown();
	}
}