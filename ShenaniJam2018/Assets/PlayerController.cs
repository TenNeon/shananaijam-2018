﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	static Vector2 startPosition;
	static PlayerController main;
	// Use this for initialization
	void Start () {
		if (main == null)
		{
			main = this;
		}
		else
		{
			Destroy(this);
		}
		startPosition = transform.position;
	}
	
	public static void Respawn()
	{
		main.transform.position = startPosition;
	}

}
