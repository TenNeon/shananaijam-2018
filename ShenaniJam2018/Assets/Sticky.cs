﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sticky : MonoBehaviour {
	Rigidbody2D rb;
	[SerializeField]
	float stickiness = 0.1f;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D>();
		if (rb == null)
		{
			rb = gameObject.AddComponent<Rigidbody2D>();
			print("added rb");
		}
		else
		{
			print("have rb");
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnCollisionStay2D(Collision2D collision)
	{
		var otherRb = collision.collider.GetComponent<Rigidbody2D>();
		if (otherRb == null)
		{
			return;
		}
		var otherSticky = collision.collider.gameObject.GetComponent<Sticky>();
		if (otherSticky == null)
		{
			return;
		}
		if (rb == null)
		{
			print("???");
		}


		var velocity = rb.velocity;
		var rotation = rb.angularVelocity;
		var otherVelocity = otherRb.velocity;
		var otherRotation = otherRb.angularVelocity;

		var otherMass = otherRb.mass;

		var force = velocity + otherVelocity;
		var torque = rotation + otherRotation;

		rb.AddForce(force * stickiness);
		rb.AddTorque(torque * stickiness);

	}
}
