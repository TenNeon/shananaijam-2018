﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldSpin : MonoBehaviour {
	Rigidbody2D rb;
	[SerializeField]
	float angleChange = 0.2f;
	public static WorldSpin main;
	static bool rotateCW = false;
	static bool sleep = false;

	// Use this for initialization
	void Start()
	{
		if (main == null)
		{
			main = this;
		}
		else
		{
			Destroy(gameObject);
		}

	}

	// Update is called once per frame
	void FixedUpdate()
	{
		if (sleep)
		{
			return;
		}
		if (rotateCW)
		{
			transform.Rotate(Vector3.back, angleChange);
		}
		else
		{
			transform.Rotate(Vector3.back, -angleChange);
		}
	}

	public static void RotateCCW()
	{
		rotateCW = false;
	}

	public static void RotateCW()
	{
		rotateCW = true;
	}

	public static void Sleep()
	{
		sleep = !sleep;
	}
}
