﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spike : MonoBehaviour {

	private void OnTriggerEnter2D(Collider2D collision)
	{
		var player = collision.gameObject.GetComponent<PlayerController>();
		if (player != null)
		{
			if (PrefabKeeper.prefabs.ContainsKey("body"))
			{
				var bodyPrefab = PrefabKeeper.prefabs["body"];
				var pos = player.transform.position;
				var rot = player.transform.rotation;

				var bodyObj = Instantiate(bodyPrefab);
				bodyObj.transform.position = pos;
				bodyObj.transform.rotation = rot;
			}
			
			PlayerController.Respawn();
		}
	}
}
