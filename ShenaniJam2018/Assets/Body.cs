﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Body : MonoBehaviour {
	float spawnTime;
	float maxLife = 60f * 5f;
	// Use this for initialization
	void Start () {
		spawnTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time > spawnTime + maxLife)
		{
			Destroy(gameObject);
		}
	}
}
