﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour {
	public static void Collapse()
	{
		var level = GameObject.Find("Level").transform;
		foreach (Transform obj in level)
		{
			var rb = obj.gameObject.GetComponent<Rigidbody2D>();
			if (rb == null)
			{
				rb = obj.gameObject.AddComponent<Rigidbody2D>();
			}
			else
			{
				return;
			}
			rb.gravityScale = 0;
			rb.AddForce(Random.onUnitSphere*500f);
			rb.AddTorque(Random.Range(-500,500));
		}
		level.DetachChildren();
	}

}
