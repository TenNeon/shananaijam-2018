﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {
	[SerializeField]
	List<GameObject> spawnableObjects = new List<GameObject>();
	List<GameObject> spawnedObjects = new List<GameObject>();
	[SerializeField]
	float cooldown = 1f;
	float lastSpawnTime = 0f;
	[SerializeField]
	int maxObjects = 100;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time > lastSpawnTime + cooldown)
		{
			Spawn();
		}
	}

	void Spawn()
	{
		if (spawnedObjects.Count < maxObjects)
		{
			var obj = Instantiate(spawnableObjects[0]);
			var pos = Random.onUnitSphere * 0.5f;
			pos.z = 0;
			pos.y += 5f;
			obj.transform.position = pos; ;
			spawnedObjects.Add(obj);
			lastSpawnTime = Time.time;
		}	
	}

}
