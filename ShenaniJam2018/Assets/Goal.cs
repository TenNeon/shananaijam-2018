﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour {
	public string targetLevel = "level1";
	public bool isLastLevel = false;
	private void OnTriggerEnter2D(Collider2D collision)
	{
		var player = collision.gameObject.GetComponent<PlayerController>();
		if (player != null)
		{
			Destroy(player);
			StartCoroutine("LoadScene");
		}
	}

	IEnumerator LoadScene()
	{
		Time.timeScale = 1f;
		WorldSpin.Sleep();
		LevelController.Collapse();
		yield return new WaitForSecondsRealtime(3f);
		Time.timeScale = 1f;
		WorldSpin.Sleep();
		if (isLastLevel)
		{
			Game.Win();
		}
		SceneManager.LoadScene(targetLevel);
	}

}
